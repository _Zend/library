﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;
using Library.Commands;
using Library.Models;

namespace Library.ViewModels
{
    internal class BookViewModel  : INotifyPropertyChanged
    {
        private AppContext Database { get; }

        private Book _book;

        public Book Book
        {
            get => _book;
            set
            {
                _book = value;
                OnPropertyChanged("Book");
            }
        }

        public RelayCommand AddCommand
        {
            get
            {
                return new RelayCommand(t =>
                {
                    if (Validator.TryValidateObject(Book, new ValidationContext(Book, null, null), null) == false)
                    {
                        MessageBox.Show("Incorrect book fields.");
                        return;
                    }
                    Database.Books.Add(Book);
                    Database.SaveChanges();
                    Update();
                });
            }
        }

        private void Update()
        {
            Books.Add(Book);
            Book = new Book() { ReleaseDate = DateTime.Today };
        }

        public ObservableCollection<Book> Books { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string property = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        
        public BookViewModel()
        {
            Database = new AppContext();
            Books = new ObservableCollection<Book>(Database.Books);
            Book = new Book() {ReleaseDate = DateTime.Today};
        }
    }
}
