using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using Library.Models;

namespace Library.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Library.Models.AppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Library.Models.AppContext context)
        {
            var books = new List<Book>()
            {
                new Book() {Author = "�����1", ReleaseDate = DateTime.UtcNow, Title = "�����1"},
                new Book() {Author = "�����2", ReleaseDate = DateTime.UtcNow, Title = "�����2"},
                new Book() {Author = "�����3", ReleaseDate = DateTime.UtcNow, Title = "�����3"}
            };

            context.Books.AddRange(books);
        }
    }
}
