﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Library.Models
{
    public class AppContext : DbContext
    {
        public DbSet<Book> Books { get; set; }

        public AppContext() : base("BooksDB")
        {

        }
    }
}
