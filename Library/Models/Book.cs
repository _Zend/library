﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Library.Models
{
    public class Book : INotifyPropertyChanged
    {
        [Key]
        public int Id { get; set; }

        private string _title;

        private string _author;

        private DateTime _releasedate;

        [Required]
        [MinLength(3)]
        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                OnPropertyChanged("Title");
            }
        }

        [Required]
        [MinLength(3)]
        public string Author
        {
            get => _author;
            set
            {
                _author = value;
                OnPropertyChanged("Author");
            }
        }

        [Required]
        public DateTime ReleaseDate
        {
            get => _releasedate;
            set
            {
                _releasedate = value;
                OnPropertyChanged("ReleaseDate");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
